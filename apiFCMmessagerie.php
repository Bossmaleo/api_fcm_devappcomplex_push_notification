<?php
        // Enabling error reporting
        error_reporting(-1);
        ini_set('display_errors', 'On');
		$_GET['push_type'] = "individual";

        require_once __DIR__ . '/firebase.php';
        require_once __DIR__ . '/push.php';

        $firebase = new Firebase();
        $push = new Push();

        // optional payload
        $payload = array();
        $payload['team'] = 'India';
        $payload['score'] = '5.6';

        // notification title
        $title = isset($_GET['title']) ? $_GET['title'] : '';
        
        //on construit le json de la pushnotification
        $_GET['regId'] = 'e-R2rM4F7cg:APA91bFhSAjcSH-k1yd4Pa6Q7CC6Nq-N0HXhWjO7RWLRcMrLSCM1a7A3ZSLhaph_BidOJenaDUhHFjliyxe4KE-Otg1szKR1I9NI6Vc7cPkC7L7DXQ9BgeK1SNZvH3Z7kXfmyMgxIkDx';
        $_GET['message']="MALEO SAMA Le GOAT";
        $_GET['title'] = "StopCovid";
        // notification message
        $message = isset($_GET['message']) ? $_GET['message'] : '';

        // push type - single user / topic
        $push_type = isset($_GET['push_type']) ? $_GET['push_type'] : '';

        // whether to include to image or not
        $include_image = isset($_GET['include_image']) ? TRUE : FALSE;


        $push->setTitle($title);
        $push->setMessage($message);
        if ($include_image) {
            $push->setImage('http://api.androidhive.info/images/minion.jpg');
        } else {
            $push->setImage('');
        }
        $push->setIsBackground(FALSE);
        $push->setPayload($payload);


        $json = '';
        $response = '';

        if ($push_type == 'topic') {
            $json = $push->getPush();
            $response = $firebase->sendToTopic('global', $json);
        } else if ($push_type == 'individual') {
            $json = $push->getPush();
            $regId = isset($_GET['regId']) ? $_GET['regId'] : '';
			$res = array();
			$res['title'] = $_GET['title'];
			$res['body'] = $_GET['message'];
            $response = $firebase->send($regId,$res);
        }
        ?>
